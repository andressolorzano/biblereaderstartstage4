package bibleReader;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import bibleReader.model.ArrayListBible;
import bibleReader.model.Bible;
import bibleReader.model.BibleReaderModel;
import bibleReader.model.VerseList;

/**
 * The main class for the Bible Reader Application.
 * 
 * @author cusack
 * @author Andrew Arent & Dre Solorzano
 */
public class BibleReaderApp extends JFrame {
	// Change these to suit your needs.
	public static final int width = 600;
	public static final int height = 600;

	public static void main(String[] args) {
		new BibleReaderApp();
	}

	// Fields
	private BibleReaderModel model;
	private ResultView resultView;

	// text within search bar
	private String text;
	// JStuff that allows interaction
	private JFrame mainFrame;
	private JButton search;
	private JTextField searchBar;
	private JPanel inputPanel;

	/**
	 * Default constructor. We may want to replace this with a different one.
	 */
	public BibleReaderApp() {
		// There is no guarantee that this complete/correct, so take a close
		// look to make sure you understand what this code is doing in case
		// you need to modify or add to it.
		model = new BibleReaderModel(); // For now call the default constructor. This might change.
		File kjvFile = new File("kjv.atv");
		VerseList verses = BibleIO.readBible(kjvFile);

		Bible kjv = new ArrayListBible(verses);

		model.addBible(kjv);

		resultView = new ResultView(model);

		setupGUI();
		pack();
		mainFrame.setSize(width, height);

	}

	/**
	 * Set up the main GUI. Make sure you don't forget to put resultView somewhere!
	 */
	private void setupGUI() {
		// Create JFrame
		mainFrame = new JFrame();

		// Add listener that closes the program when the window is shut
		mainFrame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});

		// Create JButton and add action listener
		search = new JButton("Search");
		search.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Search the text within the searchBar
				resultView.Search(text);
			}
		});

		// Create the SearchBar and add listener
		searchBar = new JTextField(20);
		searchBar.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent e) {
				// Update text every time a key is pressed
				text = searchBar.getText();
			};
		});

		// Panel of JStuff to put them all into one borderLayout position
		inputPanel = new JPanel();
		inputPanel.add(search);
		inputPanel.add(searchBar);

		// Create and fill the borderLayout
		Container contents = mainFrame.getContentPane();
		contents.add(resultView, BorderLayout.CENTER);
		contents.add(inputPanel, BorderLayout.NORTH);
		mainFrame.setVisible(true);

		// The stage numbers below may change, so make sure to pay attention to
		// what the assignment says.
		// TODO Add passage lookup: Stage ?
		// TODO Add 2nd version on display: Stage ?
		// TODO Limit the displayed search results to 20 at a time: Stage ?
		// TODO Add 3rd versions on display: Stage ?
		// TODO Format results better: Stage ?
		// TODO Display cross references for third version: Stage ?
		// TODO Save/load search results: Stage ?
	}

}
