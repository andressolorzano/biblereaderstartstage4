package bibleReader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import bibleReader.model.Bible;
import bibleReader.model.BookOfBible;
import bibleReader.model.Verse;
import bibleReader.model.VerseList;

/**
 * A utility class that has useful methods to read/write Bibles and Verses.
 * 
 * @author cusack
 */
public class BibleIO {

	/**
	 * Read in a file and create a Bible object from it and return it.
	 * 
	 * @param bibleFile
	 * @return
	 */
	// This method is complete, but it won't work until the methods it uses are
	// implemented.
	public static VerseList readBible(File bibleFile) { // Get the extension of
														// the file
		String name = bibleFile.getName();
		String extension = name.substring(name.lastIndexOf('.') + 1, name.length());

		// Call the read method based on the file type.
		if ("atv".equals(extension.toLowerCase())) {
			return readATV(bibleFile);
		} else if ("xmv".equals(extension.toLowerCase())) {
			return readXMV(bibleFile);
		} else {
			return null;
		}
	}

	/**
	 * Read in a Bible that is saved in the "ATV" format. The format is described
	 * below.
	 * 
	 * @param bibleFile The file containing a Bible with .atv extension.
	 * @return A Bible object constructed from the file bibleFile, or null if there
	 *         was an error reading the file.
	 */
	private static VerseList readATV(File bibleFile) {
		// local variables to store the strings of the bibleFile
		String abbrev = "";
		String title = "";
		String firstline = "";
		String[] line;
		try {
			FileReader fileReader = new FileReader(bibleFile);
			BufferedReader br = new BufferedReader(fileReader);
			firstline = br.readLine();
			line = firstline.split(":");
			// if the first line was unable to be split at a colon
			if (line.length < 2) {
				// if the first line was empty
				if (firstline.equals("")) {
					abbrev = "unknown";
					title = "";
				}
				// if the first line wasn't empty and wasn't able to be split
				else {
					abbrev = line[0].trim();
					title = "";
				}
			}
			// if the line was able to be split at a colon
			else {
				abbrev = line[0].trim();
				title = line[1].trim();

			}
			VerseList v = new VerseList(abbrev, title);
			// while there are lines able to be read
			while (br.ready()) {
				try {
					// split the lines at @ and the first :
					String[] contents = br.readLine().split("@");
					String[] chapAndVerse = contents[1].split(":");
					// if the abbreviation for the BookOfBible is not real, close reader and return null
					if (BookOfBible.getBookOfBible(contents[0]) == null) {
						br.close();
						return null;
					}
					BookOfBible b = BookOfBible.getBookOfBible(contents[0]);
					Verse verse = new Verse(b, Integer.parseInt(chapAndVerse[0]), Integer.parseInt(chapAndVerse[1]),
							contents[2]);
					v.add(verse);
				// if any of the variables for the creation of the verse created an error, 
				// close the reader and return null
				} catch (RuntimeException e) {
					br.close();
					return null;
				}
			}
			// successful verses being created will be returned and the reader will be closed
			br.close();
			return v;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}


	/**
	 * Read in the Bible that is stored in the XMV format. the Format is described below.
	 * 
	 * @param bibleFile The file containing a Bible with .xmv extension.
	 * @return A Bible object constructed from the file bibleFile, or null if there
	 *         was an error reading the file.
	 */
	private static VerseList readXMV(File bibleFile) {
		String chapter = "";
		String book = "";
		BookOfBible b = null;
		String verse = "";
		String text = "";
		String title = "";
		String firstline = "";
		String [] line;
		String newLine = "";
		String abbrev = "";	
		try {
			FileReader fileReader = new FileReader(bibleFile);
			BufferedReader br = new BufferedReader(fileReader);	
			firstline = br.readLine().trim();
			newLine = firstline;
			line = firstline.split(":", 2);
			// if the first line was unable to be split at a colon
			if (line.length < 2) {
				// if the first line was empty
				if (firstline.equals("")) {
					abbrev = "unknown";
					title = "";
				}
				// if the first line wasn't empty and wasn't able to be split
				else {
					abbrev = line[0].trim();
					title = "";
				}		
			}
			// if the line was able to be split at a colon
			else {
				String [] abbreviation = line[0].split(" ");
				abbrev = abbreviation[1].trim();
				title = line[1].trim();
			}
			
			VerseList verseList = new VerseList(abbrev, title);
			while (br.ready()) {
				try {
					newLine = br.readLine().trim();
					// check to see if it is a book line
					if (newLine.startsWith("<Book")) {
						line = newLine.split(",");
						String [] bookName = line[0].split("Book");
						book = bookName[1].toLowerCase().trim();
						book.replaceAll("\\s+","");
						// check if the book is an actual book
						if (BookOfBible.getBookOfBible(book) == null) {
							br.close();
							return null;
						}
						else {
							b = BookOfBible.getBookOfBible(book);
						}
					}
					// check to see if it is a chapter line
					if (newLine.startsWith("<Chapter")) {
						String [] chapterValue = newLine.split(" ");
						chapter = chapterValue[1].replace(">", "").trim();
					}
					// check to see if it is a Verse line
					if (newLine.startsWith("<Verse")) {
						line = newLine.split(">");
						String [] verseValue = line[0].split(" ");
						verse = verseValue[1];
						text = line[1];
						Verse v = new Verse(b, Integer.parseInt(chapter), Integer.parseInt(verse), 
								text);
						verseList.add(v);
					}
					// if any of the variables for the creation of the verse created an error, 
					// close the reader and return null
				} catch (RuntimeException e) {
					br.close();
					return null;
				}
				

			}
			// successful verses being created will be returned and the reader will be closed
			br.close();
			return verseList;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Write out the Bible in the ATV format.
	 * 
	 * @param file  The file that the Bible should be written to.
	 * @param bible The Bible that will be written to the file.
	 */
	public static void writeBibleATV(File file, Bible bible) {
		try {
			FileWriter outStream = new FileWriter(file);
			PrintWriter printWriter = new PrintWriter(outStream);
			String version = bible.getVersion();
			String title = bible.getTitle();
			String description = version + ": " + title;
			writeVersesATV(file, description, bible.getAllVerses());
			printWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Write out the given verses in the ATV format, using the description as the
	 * first line of the file.
	 * 
	 * @param file        The file that the Bible should be written to.
	 * @param description The contents that will be placed on the first line of the
	 *                    file, formatted appropriately.
	 * @param verses      The verses that will be written to the file.
	 */
	public static void writeVersesATV(File file, String description, VerseList verses) {
		try {
			FileWriter outStream = new FileWriter(file);
			PrintWriter printWriter = new PrintWriter(outStream);
			printWriter.println(description);
			for (Verse verse: verses) {
				String book = verse.getReference().getBook();
				int chapter = verse.getReference().getChapter();
				int vers = verse.getReference().getVerse();
				String text = verse.getText();
				String line = (book + "@" + chapter + ":" + vers + "@" + text);
				printWriter.println(line);
				
			}
			printWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Write the string out to the given file. It is presumed that the string is an
	 * HTML rendering of some verses, but really it can be anything.
	 * 
	 * @param file
	 * @param text
	 */
	public static void writeText(File file, String text) {
		try {
			FileWriter outStream = new FileWriter(file);
			PrintWriter printWriter = new PrintWriter(outStream);
			printWriter.print(text);
			printWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
