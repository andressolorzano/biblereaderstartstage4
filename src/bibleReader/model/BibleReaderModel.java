package bibleReader.model;

import java.util.ArrayList;

/**
 * The model of the Bible Reader. It stores the Bibles and has methods for
 * searching for verses based on words or references.
 * 
 * @author cusack
 * @author Andrew Arent & Dre Solorzano
 */
public class BibleReaderModel implements MultiBibleModel {

	// ---------------------------------------------------------------------------
	// to store several Bible objects.
	private ArrayList<ArrayListBible> bibleList;

	/**
	 * Default constructor
	 */
	public BibleReaderModel() {
		bibleList = new ArrayList<ArrayListBible>();

	}

	@Override
	public String[] getVersions() {

		ArrayList<String> v = new ArrayList<String>();
		// loop to create an arrayList of versions
		for (ArrayListBible bible : bibleList) {
			v.add(bible.getVersion());
		}
		String versions[] = new String[v.size()];
		// loop to put all versions into a String Array
		for (int i = 0; i < v.size(); i++) {
			versions[i] = v.get(i);

		}
		return versions;
	}

	@Override
	public int getNumberOfVersions() {
		return bibleList.size();
	}

	@Override
	public void addBible(Bible bible) {
		bibleList.add((ArrayListBible) bible);

	}

	@Override
	public Bible getBible(String version) {
		// loop to search for specified bible; if it can't be found, return null
		for (ArrayListBible bible : bibleList) {
			if (bible.getVersion().equals(version)) {
				return bible;
			}
		}
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesContaining(String words) {
		ArrayList<Reference> r = new ArrayList<Reference>();
		// loop to create an arraylist that contains all references with the keyword
		for (ArrayListBible bible : bibleList) {
			ArrayList<Reference> r2 = bible.getReferencesContaining(words);
			for (Reference ref : r2) {
				r.add(ref);
			}
		}
		return r;
	}

	@Override
	public VerseList getVerses(String version, ArrayList<Reference> references) {
		// loop to iterate through all bibles
		for (ArrayListBible bible : bibleList) {
			// if the versions match, return the verses with the matching references
			if (bible.getVersion().equals(version)) {
				return bible.getVerses(references);
			}
		}
		return null;
	}
	// ---------------------------------------------------------------------

	@Override
	public String getText(String version, Reference reference) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesForPassage(String reference) {
		// TODO Implement me: Stage 7
		return null;
	}

	// -----------------------------------------------------------------------------
	// The next set of methods are for use by the getReferencesForPassage method
	// above.
	// After it parses the input string it will call one of these.
	//
	// These methods should be somewhat easy to implement. They are kind of delegate
	// methods in that they call a method on the Bible class to do most of the work.
	// However, they need to do so for every version of the Bible stored in the
	// model.
	// and combine the results.
	//
	// Once you implement one of these, the rest of them should be fairly
	// straightforward.
	// Think before you code, get one to work, and then implement the rest based on
	// that one.
	// -----------------------------------------------------------------------------

	@Override
	public ArrayList<Reference> getVerseReferences(BookOfBible book, int chapter, int verse) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getPassageReferences(Reference startVerse, Reference endVerse) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getBookReferences(BookOfBible book) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getChapterReferences(BookOfBible book, int chapter) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getChapterReferences(BookOfBible book, int chapter1, int chapter2) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getPassageReferences(BookOfBible book, int chapter, int verse1, int verse2) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getPassageReferences(BookOfBible book, int chapter1, int verse1, int chapter2,
			int verse2) {
		// TODO Implement me: Stage 7
		return null;
	}

	// ------------------------------------------------------------------
	// These are the better searching methods.
	//
	@Override
	public ArrayList<Reference> getReferencesContainingWord(String word) {
		// TODO Implement me: Stage 12
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesContainingAllWords(String words) {
		// TODO Implement me: Stage 12
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesContainingAllWordsAndPhrases(String words) {
		// TODO Implement me: Stage 12
		return null;
	}
}
