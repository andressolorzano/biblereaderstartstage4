package bibleReader.model;

import java.util.ArrayList;

/**
 * A class that stores a version of the Bible.
 * 
 * @author Chuck Cusack (Provided the interface). Modified February 9, 2015.
 * @author Dre Solorzano (provided the implementation)
 */
public class ArrayListBible implements Bible {

	// list of all the verses in the bible
	ArrayList<Verse> verses;
	// version of the bible
	String version;
	// title of the bible
	String title;

	/**
	 * Create a new Bible with the given verses.
	 * 
	 * @param verses All of the verses of this version of the Bible.
	 */
	public ArrayListBible(VerseList verses) {
		this.verses = new ArrayList<Verse>(verses.copyVerses());
		version = verses.getVersion();
		title = verses.getDescription();
	}

	@Override
	public int getNumberOfVerses() {
		return verses.size();
	}

	@Override
	public String getVersion() {
		return version;
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public boolean isValid(Reference ref) {
		// for loop to search through all verses in the array list to check if
		// the reference is equal to any of the verses' references
		for (Verse verse : verses) {
			if (verse.getReference().equals(ref)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String getVerseText(Reference r) {
		// for loop to find the verse based on the reference
		// if found, returns the text of the verse. If not, returns null
		for (Verse verse : verses) {
			if (verse.getReference().equals(r)) {
				return verse.getText();
			}
		}
		return null;
	}

	@Override
	public Verse getVerse(Reference r) {
		// for loop to find the verse based on the reference
		// if found, returns verse. If not, returns null
		for (Verse verse : verses) {
			if (verse.getReference().equals(r)) {
				return verse;
			}
		}
		return null;
	}

	@Override
	public Verse getVerse(BookOfBible book, int chapter, int verse) {
		// Creates a reference from the parameters, then uses the getVerse method
		Reference ref = new Reference(book, chapter, verse);
		return getVerse(ref);
	}

	// ---------------------------------------------------------------------------------------------
	// The following part of this class should be implemented for stage 4.
	// See the Bible interface for the documentation of these methods.
	// Do not over think these methods. All three should be pretty
	// straightforward to implement.
	// For Stage 8 (give or take a 1 or 2) you will re-implement them so they
	// work better.
	// At that stage you will create another class to facilitate searching and
	// use it here.
	// (Essentially these two methods will be delegate methods.)
	// ---------------------------------------------------------------------------------------------

	@Override
	public VerseList getAllVerses() {
		VerseList v = new VerseList(version, title);
		for (Verse verse : verses) {
			v.add(verse);
		}
		return v;
	}

	@Override
	public VerseList getVersesContaining(String phrase) {
		VerseList v = new VerseList(version, phrase);
		// if the phrase is not an empty string
		if (!phrase.equals("")) {
			// loop to check every verse (not case sensitive nor space sensitive to its
			// surroundings)
			for (Verse verse : verses) {
				if (verse.getText().trim().toLowerCase().contains(phrase.trim().toLowerCase())) {
					v.add(verse);
				}
			}
		}
		return v;
	}

	@Override
	public ArrayList<Reference> getReferencesContaining(String phrase) {
		ArrayList<Reference> r = new ArrayList<>();
		// if the phrase is not an empty string
		if (!phrase.equals("")) {
			// loop to check every verse (not case sensitive nor space sensitive to its
			// surroundings)
			for (Verse verse : verses) {
				if (verse.getText().trim().toLowerCase().contains(phrase.trim().toLowerCase())) {
					r.add(verse.getReference());
				}
			}
		}
		return r;

	}

	@Override
	public VerseList getVerses(ArrayList<Reference> references) {
		VerseList v = new VerseList(version, "Arbitrary list of Verses");
		// boolean to state whether or not the reference was found
		boolean found = false;
		// loop to check every reference in every verse
		for (Reference reference : references) {
			for (Verse verse : verses) {
				// if reference is found, add to VerseList and change found to true
				if (verse.getReference().equals(reference)) {
					v.add(verse);
					found = true;
				}
			}
			// if reference is not found, add null to VerseList
			if (!found) {
				v.add(null);
			}
			// reset boolean to false
			found = false;
		}
		return v;
	}
	// ---------------------------------------------------------------------------------------------
	// The following part of this class should be implemented for Stage 7.
	//
	// HINT: Do not reinvent the wheel. Some of these methods can be implemented
	// by looking up
	// one or two things and calling another method to do the bulk of the work.
	// ---------------------------------------------------------------------------------------------

	@Override
	public int getLastVerseNumber(BookOfBible book, int chapter) {
		// TODO Implement me: Stage 7
		return -1;
	}

	@Override
	public int getLastChapterNumber(BookOfBible book) {
		// TODO Implement me: Stage 7
		return -1;
	}

	@Override
	public ArrayList<Reference> getReferencesInclusive(Reference firstVerse, Reference lastVerse) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesExclusive(Reference firstVerse, Reference lastVerse) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesForBook(BookOfBible book) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesForChapter(BookOfBible book, int chapter) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesForChapters(BookOfBible book, int chapter1, int chapter2) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesForPassage(BookOfBible book, int chapter, int verse1, int verse2) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesForPassage(BookOfBible book, int chapter1, int verse1, int chapter2,
			int verse2) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public VerseList getVersesInclusive(Reference firstVerse, Reference lastVerse) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public VerseList getVersesExclusive(Reference firstVerse, Reference lastVerse) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public VerseList getBook(BookOfBible book) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public VerseList getChapter(BookOfBible book, int chapter) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public VerseList getChapters(BookOfBible book, int chapter1, int chapter2) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public VerseList getPassage(BookOfBible book, int chapter, int verse1, int verse2) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public VerseList getPassage(BookOfBible book, int chapter1, int verse1, int chapter2, int verse2) {
		// TODO Implement me: Stage 7
		return null;
	}
}
