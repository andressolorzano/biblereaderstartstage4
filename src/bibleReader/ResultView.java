package bibleReader;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import bibleReader.model.ArrayListBible;
import bibleReader.model.BibleReaderModel;
import bibleReader.model.Reference;

/**
 * The display panel for the Bible Reader.
 * 
 * @author cusack
 * @author Andrew Arent & Dre Solorzano
 */
public class ResultView extends JPanel {
	// the model the view accesses information from
	private BibleReaderModel model;
	// ArrayList of references within the model referring to the keyword
	private ArrayList<Reference> references;
	// JStuff
	private JScrollPane scroll;
	private JTextArea verseText;
	private JTextArea VerseID;
	private JPanel layout;
	// Keyword that is being searched
	private String text;
	// ArrayList of ArrayListBibles within the model referring to the keyword
	private ArrayList<ArrayListBible> bibles;

	private ArrayList<ArrayList<Reference>> listOfReferences;

	/**
	 * Construct a new ResultView and set its model to myModel. It needs to model to
	 * look things up.
	 * 
	 * @param myModel The model this view will access to get information.
	 */
	public ResultView(BibleReaderModel myModel) {
		// Initialize fields
		model = myModel;
		bibles = new ArrayList<ArrayListBible>();
		listOfReferences = new ArrayList<ArrayList<Reference>>();
		new JPanel();
		this.setLayout(new BorderLayout());

		layout = new JPanel();
		scroll = new JScrollPane(layout, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		// Add JStuff to the ResultView JPanel and make visible
		this.add(scroll, BorderLayout.CENTER);
		this.setVisible(true);

	}

	/**
	 * Creates a list of all the references that contain the keyword, and updates
	 * the ResultView
	 * 
	 * @param text the keyword being searched for
	 */
	public void Search(String text) {
		// if there is no keyword, do not search anything
		if (!(text == null)) {

			this.text = text;
			// clear the layout and arrayList bibles
			layout.removeAll();
			bibles.clear();
			listOfReferences.clear();
			// stores an array of current versions in BibleReader
			String versions[] = model.getVersions();
			// loops through the bibles, casts the current bible to an arraylistbible, then
			// adds it to the bibles arraylist
			for (int i = 0; i < versions.length; i++) {
				bibles.add((ArrayListBible) model.getBible(versions[i]));
			}
			// storing the list of references containing text into a field-references
			for (ArrayListBible bible : bibles) {
				references = (bible.getReferencesContaining(text));
				listOfReferences.add(references);

				// results.setVisible(true);
			}
			Compiler();

		}
	}

	/**
	 * Correctly sets up the layout for the ResultView, and displays all information
	 * pertaining to the keyword
	 */
	public void Compiler() {

		// finds which bible has the most references in order to format the layout
		// properly
		int maxRefs = 0;
		for (int i = 0; i < model.getNumberOfVersions(); i++) {
			if (listOfReferences.get(maxRefs).size() < listOfReferences.get(i).size()) {
				maxRefs = i;
			}
		}

		// values for formatting the layout
		int numOfVersions = model.getNumberOfVersions();
		int numOfRefs = listOfReferences.get(maxRefs).size();

		// Formatting the layout
		layout.setLayout(new GridLayout(numOfRefs + 1, numOfVersions + 1));
		layout.add(new JTextArea("verses"));

		// loop that displays versions
		for (int i = 0; i < numOfVersions; i++) {
			layout.add(new JTextArea(model.getVersions()[i]));
		}

		// loop that iterates through all the references to get their reference and
		// their verse associated with them
		for (int i = 0; i < numOfRefs; i++) {
			layout.add(new JTextArea(listOfReferences.get(maxRefs).get(i).toString()));
			for (int j = 0; j < numOfVersions; j++) {
				String refVerse = "";
				refVerse = (bibles.get(j).getVerseText(listOfReferences.get(maxRefs).get(i)));
				layout.add(new JTextArea(refVerse));
			}
		}

		// Updates the layout to make visible
		layout.updateUI();
	}

}
